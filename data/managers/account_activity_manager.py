from data import models
from data.managers.base import SQLAlchemyManager


class AccountActivityManager(SQLAlchemyManager):
    model = models.AccountActivity

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
