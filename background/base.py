'''
    Base class for all background tasks
'''
import logging
from abc import abstractmethod

from utils.exceptions import AppWarningException
from utils.logging import setup_logging, logger, trace
from services.common.task_semaphore_service import TaskSemaphoreService


class BaseBackgroundTask:
    '''
    Base class for all background tasks
    '''
    def __init__(
        self,
        name: str,
        interval: int,
        log_filename: str,
        log_level: str,
        log_format: str,
        log_datefmt: str,
        task_semaphore_service: TaskSemaphoreService
    ):
        self.name = name
        self.interval = interval
        self.task_semaphore_service = task_semaphore_service

        self.log_filename = log_filename
        self.log_level = log_level
        self.log_format = log_format
        self.log_datefmt = log_datefmt

    async def iter(self):
        try:
            # check if task is enabled
            enabled = await self.task_semaphore_service.get_enabled(self.name)
            if not enabled:
                self.logging.warning(f'Task "{self.name}" is disabled')
                return

            # check if task needed to force
            force = await self.task_semaphore_service.get_pending(self.name)
            if force:
                self.logging.warning(f'Task "{self.name}" forced to run from outside')

            # run task iteration logic
            await self.run(force)

        except AppWarningException as err:
            self.logging.warning(f'Task {err.level} {err.detail}')
        except Exception as err:
            self.logging.error(f'Task {self.name}:{trace(err)}')
        finally:
            await self.task_semaphore_service.set_pending(self.name, False)

    @abstractmethod
    async def run(self, force: bool = False):
        raise NotImplementedError

    async def setup(self):
        # setup logging
        task_logger = setup_logging(
            filename=self.log_filename,
            level=self.log_level,
            format=self.log_format,
            datefmt=self.log_datefmt,
            logger_name=self.name,
        )
        logger.set(task_logger)

        self.logging.info(f'{self.name} has been setup for running')

    @property
    def logging(self) -> logging.Logger:
        return logger.get()
