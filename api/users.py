'''
    User endpoints
'''
from fastapi import APIRouter, Depends
from fastapi_cache.decorator import cache
from dependency_injector.wiring import inject, Provide
from fastapi.responses import StreamingResponse

from services.common.plot_service import PlotService
from utils.consts import NS_API, CACHE_1M, CACHE_2M, CACHE_10M
from container import AppContainer
from dto import OK

router = APIRouter(
    prefix='/users',
    tags=['users']
)


@router.get('/test')
@cache(expire=CACHE_1M, namespace=NS_API)
@inject
async def test():
    return OK


@router.get("/plot_all")
@inject
async def get_all_plots(plot_service: PlotService = Depends(Provide[AppContainer.plot_service])):
    plots = await plot_service.combine_images()
    return StreamingResponse(plots, media_type="image/png")

@router.get("/plot_txs")
@inject
async def get_transactions(plot_service: PlotService = Depends(Provide[AppContainer.plot_service])):
    plots = await plot_service.plot_total_transactions()
    return StreamingResponse(plots, media_type="image/png")


@router.get("/plot_fraud")
@inject
async def get_fraud_transactions(plot_service: PlotService = Depends(Provide[AppContainer.plot_service])):
    plots = await plot_service.plot_fraud_transactions()
    return StreamingResponse(plots, media_type="image/png")


@router.get("/plot_rating")
@inject
async def get_rating(plot_service: PlotService = Depends(Provide[AppContainer.plot_service])):
    plots = await plot_service.plot_node_ratings()
    return StreamingResponse(plots, media_type="image/png")
