from dto.base import BaseDTO
from datetime import datetime as dt

class TransactionCreating(BaseDTO):
    tx_hash: str
    block_number: int
    node_id: int
    from_address: str
    to_address: str | None
    value: int
    gas: int
    gas_price: int
    timestamp: dt


class Transaction(TransactionCreating):
    id: int
