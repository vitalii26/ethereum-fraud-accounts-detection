from typing import List

from sqlalchemy import select

from data import models
from data.managers.base import SQLAlchemyManager
from data.models import Transaction


class TransactionManager(SQLAlchemyManager):
    model = models.Transaction

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    async def get_transactions_by_address(self, address: str) -> List[Transaction]:
        async with self.session_factory() as session:
            query = select(Transaction).filter(
                (Transaction.from_address == address) | (Transaction.to_address == address)
            )
            result = await session.execute(query)
            return result.scalars().all()

    async def get_transactions_by_from_address(self, address: str) -> List[Transaction]:
        async with self.session_factory() as session:
            query = select(Transaction).filter(
                (Transaction.from_address == address)
            )
            result = await session.execute(query)
            return result.scalars().all()

    async def get_transactions_by_node_id(self, node_id: int) -> List[Transaction]:
        async with self.session_factory() as session:
            query = select(Transaction).filter(
                Transaction.node_id == node_id
            )
            result = await session.execute(query)
            return result.scalars().all()
