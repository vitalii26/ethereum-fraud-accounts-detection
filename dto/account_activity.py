from dto.base import BaseDTO


class AccountActivityCreating(BaseDTO):
    account_id: int
    avg_min_between_sent_tnx: float
    avg_min_between_received_tnx: float
    time_diff_between_first_and_last: float
    sent_tnx: int
    received_tnx: int
    number_of_created_contracts: int
    max_val_received: float
    avg_val_received: float
    avg_val_sent: float
    total_ether_sent: float
    total_ether_balance: float
    erc20_total_ether_received: float
    erc20_total_ether_sent: float
    erc20_total_ether_sent_contract: float
    erc20_uniq_sent_addr: int
    erc20_uniq_rec_token_name: int


class AccountActivity(AccountActivityCreating):
    id: int
