'''
    Main module.
'''
import asyncio

from dependency_injector.wiring import inject

from background.cron import Scheduler
from container import AppContainer


@inject
async def run_scheduler() -> None:
    scheduler = Scheduler()
    container = AppContainer.instance()

    block_scanner_task = container.block_scanner_task()
    scheduler.create_task(block_scanner_task)

    analyzer_task = container.analyzer_task()
    scheduler.create_task(analyzer_task)

    fraud_detector_task = container.fraud_detector_task()
    scheduler.create_task(fraud_detector_task)

    await scheduler.run_tasks()


async def main():
    # start main loop
    await run_scheduler()


if __name__ == "__main__":
    asyncio.run(main())
