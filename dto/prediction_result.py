from dto.base import BaseDTO


class PredictionResult(BaseDTO):
    account_id: int
    probability: float