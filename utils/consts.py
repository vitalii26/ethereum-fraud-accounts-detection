
# flake8: noqa
import json

# caching

NS_API = 'api'

CACHE_1M = 60
CACHE_2M = 120
CACHE_10M = 600
