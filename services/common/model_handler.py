from lightgbm import LGBMClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import joblib
from imblearn.over_sampling import SMOTE
from typing import List
import pandas as pd
import numpy as np
import copy

import dto


class ModelHandler:
    def __init__(self, model_path: str = 'lgbm_model.pkl'):
        self.model = None
        self.model_path = model_path
        self.load_model()

    def load_model(self):
        try:
            self.model = joblib.load(self.model_path)
        except FileNotFoundError:
            self.model = LGBMClassifier(boosting_type='gbdt', class_weight=None, colsample_bytree=1.0,
                                        importance_type='split', learning_rate=0.1, max_depth=-1, min_child_samples=20,
                                        min_child_weight=0.001, min_split_gain=0.0, n_estimators=100, n_jobs=-1,
                                        num_leaves=31, objective=None, random_state=1, reg_alpha=0.0, reg_lambda=0.0,
                                        subsample=1.0, subsample_for_bin=200000, subsample_freq=0)
            self.train()

    def save_model(self):
        joblib.dump(self.model, self.model_path)

    def train(self):
        init_data = pd.read_csv('./transaction_dataset.csv', delimiter=',')
        init_data.drop(['erc20_most_sent_token_type', 'erc20_most_rec_token_type'], axis=1, inplace=True)
        nRow, nCol = init_data.shape
        print(f'There are {nRow} rows and {nCol} columns \n')
        init_data.drop(['Unnamed: 0', 'index', 'address'], axis=1, inplace=True)
        print('class     : count   : percent')
        print('Fraud     : {}    : {:.2%}'.format(sum(init_data['FLAG'] == 0),
                                                  sum(init_data['FLAG'] == 0) / len(init_data['FLAG'])))
        print('Non-fraud : {}    : {:.2%}'.format(sum(init_data['FLAG'] == 1),
                                                  sum(init_data['FLAG'] == 1) / len(init_data['FLAG'])))
        print('Number of NA values in dataset', init_data.isnull().sum().sum())
        init_data.isnull().sum()
        init_data.fillna(init_data.median(), inplace=True)
        print('Number of NA values in dataset', init_data.isnull().sum().sum())
        zero_variance = init_data.var() == 0
        print('Number of columns with zero variance', zero_variance.sum())
        init_data.drop(init_data.var()[zero_variance].index, axis=1, inplace=True)

        data = copy.copy(init_data)
        drop = ['total_transactions_(including_tnx_to_create_contract)',
                'total_ether_sent_contracts',
                'max_val_sent_to_contract',
                'erc20_avg_val_rec',
                'erc20_avg_val_rec',
                'erc20_max_val_rec',
                'erc20_min_val_rec',
                'erc20_uniq_rec_contract_addr',
                'max_val_sent',
                'erc20_avg_val_sent',
                'erc20_min_val_sent',
                'erc20_max_val_sent',
                'total_erc20_tnxs',
                'avg_value_sent_to_contract',
                'unique_sent_addresses',
                'unique_received_addresses',
                'total_ether_received',
                'erc20_uniq_sent_token_name',
                'min_val_received',
                'min_val_sent',
                'erc20_uniq_rec_addr',
                'min_value_sent_to_contract',
                'erc20_uniq_sent_addr.1']
        data.drop(drop, axis=1, inplace=True)

        corr_matrix = data.corr()
        upper_tri = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(bool))
        to_drop = [column for column in upper_tri.columns if any(abs(upper_tri[column]) > 0.8)]
        data = data.drop(to_drop, axis=1)

        x = data.drop('FLAG', axis=1)
        y = data['FLAG']
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

        oversample = SMOTE()
        x_tr_resample, y_tr_resample = oversample.fit_resample(x_train, y_train)

        non_fraud = 0
        fraud = 0

        for i in y_train:
            if i == 0:
                non_fraud += 1
            else:
                fraud += 1

        new_non_fraud = 0
        new_fraud = 1

        for j in y_tr_resample:
            if j == 0:
                new_non_fraud += 1
            else:
                new_fraud += 1

        print(f'Before SMOTE \n \tNon-frauds: {non_fraud} \n \tFauds: {fraud}')
        print(f'After SMOTE \n \tNon-frauds: {new_non_fraud} \n \tFauds: {new_fraud}')

        self.model.fit(x_tr_resample, y_tr_resample)
        y_pred = self.model.predict(x_test)
        accuracy = accuracy_score(y_test, y_pred)
        self.save_model()
        return accuracy

    def predict(self, data: List[dto.AccountActivity]) -> List[dto.PredictionResult]:
        if not self.model:
            raise ValueError("Model is not loaded.")

        required_columns = ['avg_min_between_sent_tnx', 'avg_min_between_received_tnx',
                            'time_diff_between_first_and_last', 'sent_tnx', 'received_tnx',
                            'number_of_created_contracts', 'max_val_received', 'avg_val_received',
                            'avg_val_sent', 'total_ether_sent', 'total_ether_balance',
                            'erc20_total_ether_received', 'erc20_total_ether_sent',
                            'erc20_total_ether_sent_contract', 'erc20_uniq_sent_addr',
                            'erc20_uniq_rec_token_name']

        df = pd.DataFrame([d.model_dump() for d in data])
        df = df.loc[:, required_columns]
        probabilities = self.model.predict_proba(df)[:, 1]
        results = []
        for i, pred in enumerate(probabilities):
            results.append(dto.PredictionResult(
                account_id=data[i].account_id,
                probability=pred
            ))

        return results
