from sqlalchemy import String, FLOAT, DateTime
from sqlalchemy.orm import mapped_column, Mapped
from datetime import datetime as dt

from data.models.base import BaseId


class Account(BaseId):
    __tablename__ = 'accounts'

    address: Mapped[str] = mapped_column(String[48], nullable=False)
    fraud_probability: Mapped[float] = mapped_column(FLOAT, nullable=False)
    updated_at: Mapped[dt] = mapped_column(DateTime, nullable=True)
