from typing import List
 
from services.base import BaseService
from data.managers.account_activity_manager import AccountActivityManager

import dto
from utils.exceptions import AccountActivityIdNotFoundError, AccountActivityAccountIdNotFoundError


class AccountActivityService(BaseService):

    def __init__(
            self,
            account_activity_manager: AccountActivityManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.account_activity_manager = account_activity_manager

    async def get_all(self) -> List[dto.AccountActivity]:
        account_activity_models = await self.account_activity_manager.find()
        return [dto.AccountActivity.model_validate(u) for u in account_activity_models]

    async def get_by_id(self, account_activity_id: int) -> dto.AccountActivity:
        account_activity_model: dto.AccountActivity = await self.account_activity_manager.find_one_or_none(
            id=account_activity_id
        )
        if account_activity_model is None:
            raise AccountActivityIdNotFoundError(account_activity_id)

        return dto.AccountActivity.model_validate(account_activity_model)

    async def get_by_account_id(self, account_id: int) -> dto.AccountActivity:
        account_activity_model: dto.AccountActivity = await self.account_activity_manager.find_one_or_none(
            account_id=account_id
        )
        if account_activity_model is None:
            raise AccountActivityAccountIdNotFoundError(account_id)

        return dto.AccountActivity.model_validate(account_activity_model)

    async def create_or_update(self, account_activity_creating: dto.AccountActivityCreating) -> dto.AccountActivity:
        account_activity_model = await self.account_activity_manager.find_one_or_none(
            account_id=account_activity_creating.account_id
        )
        if account_activity_model is not None:
            await self.account_activity_manager.edit_one(
                id=account_activity_model.id,
                data=account_activity_creating.model_dump()
            )
            return await self.get_by_id(account_activity_model.id)

        result = await self.account_activity_manager.add_one(account_activity_creating)
        account_activity_id = result['id']

        return dto.AccountActivity(
            id=account_activity_id,
            **account_activity_creating.model_dump(convert_enums=False)
        )
