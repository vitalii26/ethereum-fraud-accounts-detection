from typing import List

from sqlalchemy.exc import IntegrityError

import dto
from data.managers.node_fraud_transaction_manager import NodeFraudTransactionManager
from services.base import BaseService
from utils.exceptions import NodeFraudTransactionIdNotFoundError


class NodeFraudTransactionService(BaseService):

    def __init__(
            self,
            node_fraud_transaction_manager: NodeFraudTransactionManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.node_fraud_transaction_manager = node_fraud_transaction_manager

    async def get_all(self) -> List[dto.NodeFraudTransaction]:
        node_fraud_transaction_models = await self.node_fraud_transaction_manager.find()
        return [dto.NodeFraudTransaction.model_validate(u) for u in node_fraud_transaction_models]

    async def get_by_id(self, node_fraud_transaction_id: int) -> dto.NodeFraudTransaction:
        node_fraud_transaction_model: dto.NodeFraudTransaction = await self.node_fraud_transaction_manager.find_one_or_none(
            id=node_fraud_transaction_id
        )
        if node_fraud_transaction_model is None:
            raise NodeFraudTransactionIdNotFoundError(node_fraud_transaction_id)

        return dto.NodeFraudTransaction.model_validate(node_fraud_transaction_model)

    async def get_by_transaction_id(self, transaction_id: int) -> dto.NodeFraudTransaction:
        node_fraud_transaction_model: dto.NodeFraudTransaction = await self.node_fraud_transaction_manager.find_one_or_none(
            transaction_id=transaction_id
        )
        if node_fraud_transaction_model is None:
            raise NodeFraudTransactionIdNotFoundError(transaction_id)

        return dto.NodeFraudTransaction.model_validate(node_fraud_transaction_model)

    async def create_or_find(
            self,
            node_fraud_transaction_creating: dto.NodeFraudTransactionCreating
    ) -> dto.NodeFraudTransaction:
        node_fraud_transaction_model = await self.node_fraud_transaction_manager.find_one_or_none(
            transaction_id=node_fraud_transaction_creating.transaction_id
        )
        if node_fraud_transaction_model is not None:
            return dto.NodeFraudTransaction.model_validate(node_fraud_transaction_model)

        result = await self.node_fraud_transaction_manager.add_one(node_fraud_transaction_creating)
        node_fraud_transaction_id = result['id']

        return dto.NodeFraudTransaction(
            id=node_fraud_transaction_id,
            **node_fraud_transaction_creating.model_dump(convert_enums=False)
        )

    async def delete(self, transaction_id: int):
        node_fraud_transaction_dto = await self.get_by_transaction_id(transaction_id)
        await self.node_fraud_transaction_manager.delete_one(id=node_fraud_transaction_dto.id)

    async def delete_by_ids(self, delete_by_ids: [int]):
        await self.node_fraud_transaction_manager.delete_by_ids(transaction_ids=delete_by_ids)

    async def create_or_find_many(self, node_fraud_transactions_creating: List[dto.NodeFraudTransactionCreating]) -> List[dto.NodeFraudTransaction]:
        existing_transactions = []
        new_transactions = []

        unique_transactions = list(
            {(tx.node_id, tx.transaction_id): tx for tx in node_fraud_transactions_creating}.values())

        for transaction_creating in unique_transactions:
            transaction_model = await self.node_fraud_transaction_manager.find_one_or_none(
                transaction_id=transaction_creating.transaction_id, node_id=transaction_creating.node_id
            )
            if transaction_model is not None:
                existing_transactions.append(dto.NodeFraudTransaction.model_validate(transaction_model))
            else:
                new_transactions.append(transaction_creating)

        if new_transactions:
            results = await self.node_fraud_transaction_manager.add_many(new_transactions)
            if len(results) == 1:
                results = [results]

            created_transactions = [
                dto.NodeFraudTransaction(
                    id=result['id'],
                    **transaction_creating.model_dump(convert_enums=False)
                )
                for result, transaction_creating in zip(results, new_transactions) if result is not None
            ]
            return existing_transactions + created_transactions

        return existing_transactions
