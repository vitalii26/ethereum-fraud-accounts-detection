from sqlalchemy import Integer
from sqlalchemy.orm import Mapped, mapped_column

from data.models.base import BaseId


class NodeFraudTransaction(BaseId):
    __tablename__ = 'node_fraud_transactions'

    node_id: Mapped[int] = mapped_column(Integer, nullable=False)
    transaction_id: Mapped[int] = mapped_column(Integer, nullable=False)
    account_id: Mapped[int] = mapped_column(Integer, nullable=False)

