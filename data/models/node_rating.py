from datetime import datetime as dt

from sqlalchemy import Float, Integer, DateTime
from sqlalchemy.orm import Mapped, mapped_column

from data.models.base import BaseId


class NodeRating(BaseId):
    __tablename__ = 'node_ratings'

    node_id: Mapped[int] = mapped_column(Integer, nullable=False)
    rating: Mapped[float] = mapped_column(Float, nullable=False)
    timestamp: Mapped[dt] = mapped_column(DateTime, nullable=False)

