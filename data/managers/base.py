'''
    SQLAlchemy manager abstraction
'''
from typing import List, TypeVar, Union

from pydantic import BaseModel
from sqlalchemy import insert, select, update, delete, asc, desc
from sqlalchemy.orm import joinedload
from sqlalchemy.ext.asyncio import async_sessionmaker

from utils.utils import enum_dump
from data.database import Database
from data.models.base import BaseId


T = TypeVar('T', bound=BaseId)


class SQLAlchemyManager:
    '''
        SQLAlchemy manager abstraction + basic CRUD implementation
    '''

    model: T = None

    def __init__(self, db: Database):
        self.session_factory = async_sessionmaker(db.engine, autoflush=False, expire_on_commit=True)

    # SELECT

    async def find(
        self,
        join_rels=False,
        sort_field: Union[str, None] = None,
        sort_order: str = 'asc',
        in_field: Union[str, None] = None,
        in_values: Union[List, None] = None,
        skip: int | None = None,
        take: int | None = None,
        **filter_by
    ) -> List[T]:
        async with self.session_factory() as session:
            query = select(self.model)

            if join_rels:
                query = query.options(joinedload('*'))

            filter_by = enum_dump(filter_by)
            query = query.filter_by(**filter_by)

            if (in_field is not None) and (in_values is not None):
                query = query.filter(getattr(self.model, in_field).in_(in_values))

            if sort_field:
                if sort_order.lower() == 'asc':
                    query = query.order_by(asc(getattr(self.model, sort_field)))
                elif sort_order.lower() == 'desc':
                    query = query.order_by(desc(getattr(self.model, sort_field)))

            # Apply pagination
            if skip:
                query = query.offset(skip)
            if take:
                query = query.limit(take)

            result = await session.execute(query)

            return result.scalars().all()

    async def find_one_or_none(
        self,
        join_rels=False,
        **filter_by
    ) -> T | None:
        async with self.session_factory() as session:
            query = select(self.model)
            if join_rels:
                query = query.options(joinedload('*'))

            filter_by = enum_dump(filter_by)
            query = query.filter_by(**filter_by)
            result = await session.execute(query)

            return result.scalars().one_or_none()

    # INSERT

    async def add_one(self, data: BaseModel) -> dict:
        async with self.session_factory() as session:
            query = insert(self.model).values(**data.model_dump()).returning(self.model.id)
            result = await session.execute(query)
            await session.commit()

            return result.mappings().first()

    async def add_many(self, datas: List[BaseModel]) -> dict:
        async with self.session_factory() as session:
            datas = [data.model_dump() for data in datas]
            query = insert(self.model).values(datas).returning(self.model.id)
            result = await session.execute(query)
            await session.commit()

            return result.mappings().first()

    # UPDATE

    # TODO edit_one -> **kwargs
    async def edit_one(self, id: int, data: dict) -> int:
        async with self.session_factory() as session:
            data = enum_dump(data)
            query = update(self.model).values(**data).filter_by(id=id).returning(self.model.id)
            result = await session.execute(query)
            await session.commit()

            return result.mappings().first()

    # DELETE

    async def delete_one(self, **filter_by):
        async with self.session_factory() as session:
            filter_by = enum_dump(filter_by)
            query = delete(self.model).filter_by(**filter_by)
            await session.execute(query)
            await session.commit()
