from background.base import BaseBackgroundTask
from services.common.analyst_service import AnalystService


class Analyzer(BaseBackgroundTask):

    def __init__(
            self,
            analyst_service: AnalystService,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.analyst_service = analyst_service

    async def run(self, force: bool = False):
        self.logging.info(f'Analyzer: start iteration (force={force})')

        await self.analyst_service.collect_account_activities()

        self.logging.info('Analyzer: end iteration')
