from typing import List

import dto
from data.managers.node_manager import NodeManager
from services.base import BaseService
from utils.exceptions import NodeIdNotFoundError, NodeNotFoundError


class NodeService(BaseService):

    def __init__(
            self,
            node_manager: NodeManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.node_manager = node_manager

    async def get_all(self) -> List[dto.Node]:
        node_models = await self.node_manager.find()
        return [dto.Node.model_validate(u) for u in node_models]

    async def get_by_id(self, node_id: int) -> dto.Node:
        node_model: dto.Node = await self.node_manager.find_one_or_none(
            id=node_id
        )
        if node_model is None:
            raise NodeIdNotFoundError(node_id)

        return dto.Node.model_validate(node_model)

    async def get_by_address(self, address: str) -> dto.Node:
        node_model: dto.Node = await self.node_manager.find_one_or_none(
            address=address
        )
        if node_model is None:
            raise NodeNotFoundError()

        return dto.Node.model_validate(node_model)

    async def create_or_find(self, address: str) -> dto.Node:
        node_model = await self.node_manager.find_one_or_none(
            address=address
        )
        if node_model is not None:
            return dto.Node.model_validate(node_model)

        node_creating = dto.NodeCreating(
            address=address
        )

        result = await self.node_manager.add_one(node_creating)
        node_id = result['id']

        return dto.Node(
            id=node_id,
            **node_creating.model_dump(convert_enums=False)
        )
