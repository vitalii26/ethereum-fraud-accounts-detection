'''
    DB base model
'''
from sqlalchemy.orm import DeclarativeBase


class BaseWithoutId(DeclarativeBase):
    '''
        Database table base model
    '''
