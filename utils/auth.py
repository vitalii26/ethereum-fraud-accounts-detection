'''
    Authentication logic
'''
from fastapi.security import APIKeyHeader
from fastapi import Security, HTTPException
from dependency_injector import providers
from starlette.status import HTTP_403_FORBIDDEN


config = providers.Configuration(yaml_files=['config.yml'], strict=True)
config.load()

api_key_header = APIKeyHeader(name='access_token', auto_error=False)


async def get_api_key(api_key: str = Security(api_key_header)):
    if api_key == config.app.api_key():
        return api_key
    else:
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN,
            detail='Invalid API KEY'
        )
