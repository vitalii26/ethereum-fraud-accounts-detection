'''
    Application module.
'''
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend

from utils.consts import NS_API
from utils.logging import setup_logging, logger
from container import AppContainer
import api


def create_app() -> FastAPI:
    # create app DI container
    container = AppContainer.instance()

    # setup logging
    api_logger = setup_logging(
        filename=container.config.app.filename(),
        level=container.config.logging.level(),
        format=container.config.logging.format(),
        datefmt=container.config.logging.datefmt(),
    )
    logger.set(api_logger)

    # create API app
    app = FastAPI(**container.config.app())

    # routers
    app.include_router(api.users.router)
    app.include_router(api.admin.router)

    # middlewares
    app.add_middleware(
        CORSMiddleware,
        allow_origins=container.config.server.cors(),
        allow_credentials=True,
        allow_methods=['GET', 'POST', 'OPTIONS', 'DELETE', 'PATCH', 'PUT'],
        allow_headers=[
            'Content-Type', 'Set-Cookie', 'Access-Control-Allow-Headers',
            'Access-Control-Allow-Origin', 'Authorization'],
    )

    return app


app = create_app()


@app.on_event('startup')
async def before_startup():
    ''' App startup callback '''
    FastAPICache.init(InMemoryBackend())
    await FastAPICache.clear(NS_API)
    logger.get().info('application started')


@app.on_event('shutdown')
async def after_shutdown():
    ''' App shutdown callback '''
    await FastAPICache.clear(NS_API)
    logger.get().info('application finished')
