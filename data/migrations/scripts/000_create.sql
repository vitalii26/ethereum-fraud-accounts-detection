-- Initial create DB script

CREATE TABLE IF NOT EXISTS blocks (
    block_number bigint PRIMARY KEY,
    block_hash VARCHAR(128),
    parent_hash VARCHAR(128),
    timestamp TIMESTAMP,
    miner TEXT,
    transactions_count bigint
);

CREATE TABLE IF NOT EXISTS nodes (
    id SERIAL PRIMARY KEY,
    address VARCHAR(48)
);

CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY,
    tx_hash VARCHAR(128),
    block_number bigint,
    from_address VARCHAR(48),
    to_address VARCHAR(48),
    value NUMERIC,
    gas NUMERIC,
    gas_price NUMERIC,
    timestamp TIMESTAMP,
    node_id INTEGER,
    FOREIGN KEY (node_id) REFERENCES nodes (id),
    FOREIGN KEY (block_number) REFERENCES blocks (block_number)
);

CREATE TABLE IF NOT EXISTS accounts (
    id SERIAL PRIMARY KEY,
    address VARCHAR(48),
    fraud_probability FLOAT
);

CREATE TABLE IF NOT EXISTS account_activities (
    id SERIAL PRIMARY KEY,
    account_id INTEGER,
    avg_min_between_sent_tnx FLOAT,
    avg_min_between_received_tnx FLOAT,
    time_diff_between_first_and_last FLOAT,
    sent_tnx INTEGER,
    received_tnx INTEGER,
    number_of_created_contracts INTEGER,
    max_val_received FLOAT,
    avg_val_received FLOAT,
    avg_val_sent FLOAT,
    total_ether_sent FLOAT,
    total_ether_balance FLOAT,
    erc20_total_ether_received FLOAT,
    erc20_total_ether_sent FLOAT,
    erc20_total_ether_sent_contract FLOAT,
    erc20_uniq_sent_addr FLOAT,
    erc20_uniq_rec_token_name FLOAT,
    FOREIGN KEY (account_id) REFERENCES accounts (id)
);


CREATE TABLE IF NOT EXISTS node_ratings (
    id SERIAL PRIMARY KEY,
    node_id INTEGER,
    rating FLOAT,
    timestamp TIMESTAMP,
    FOREIGN KEY (node_id) REFERENCES nodes (id)
);

CREATE TABLE IF NOT EXISTS node_fraud_transactions (
    id SERIAL PRIMARY KEY,
    node_id INTEGER,
    transaction_id INTEGER,
    account_id INTEGER,
    FOREIGN KEY (node_id) REFERENCES nodes (id),
    FOREIGN KEY (transaction_id) REFERENCES transactions (id)
    FOREIGN KEY (account_id) REFERENCES accounts (id)
);

CREATE TABLE task_semaphores (
  id                SERIAL PRIMARY KEY,
  name              VARCHAR(64),
  pending           BOOLEAN
);

INSERT INTO task_semaphores (name, pending) VALUES
    ('BlockScanner', False);