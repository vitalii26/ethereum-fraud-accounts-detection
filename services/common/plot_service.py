import dto
from services.base import BaseService
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
from io import BytesIO
from PIL import Image

from services.common.node_fraud_transaction_service import NodeFraudTransactionService
from services.common.node_rating_service import NodeRatingService
from services.common.transaction_service import TransactionService


class PlotService(BaseService):

    def __init__(
            self,
            node_rating_service: NodeRatingService,
            transaction_service: TransactionService,
            node_fraud_transaction_service: NodeFraudTransactionService,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.node_rating_service = node_rating_service
        self.transaction_service = transaction_service
        self.node_fraud_transaction_service = node_fraud_transaction_service

    async def plot_node_ratings(self) -> BytesIO:
        ratings = await self.node_rating_service.get_all()
        if not ratings:
            fields = [field.alias for field in dto.NodeRating.model_fields.values()]
            df = pd.DataFrame(columns=fields)
        else:
            df = pd.DataFrame([vars(r) for r in ratings])
            df['timestamp'] = pd.to_datetime(df['timestamp'], errors='coerce')
            df.sort_values(by='timestamp', inplace=True)

        plt.figure(figsize=(10, 6))
        if not df.empty:
            for node_id, group in df.groupby('node_id'):
                plt.plot(group['timestamp'], group['rating'], marker='o', label=f'Node {node_id}')
            plt.legend()

        date_form = DateFormatter("%Y-%m-%d %H:%M")
        plt.gca().xaxis.set_major_formatter(date_form)

        plt.xlabel('Timestamp')
        plt.ylabel('Rating, %')
        plt.title('Node Ratings Over Time')
        plt.grid(True)

        buf = BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        plt.close()
        return buf

    async def plot_total_transactions(self) -> BytesIO:
        transactions = await self.transaction_service.get_all()
        if not transactions:
            fields = [field.alias for field in dto.Transaction.model_fields.values()]
            df = pd.DataFrame(columns=fields)
        else:
            df = pd.DataFrame([vars(t) for t in transactions])

        if df.empty or df['node_id'].isnull().all():
            total_transactions = pd.Series(dtype='int')
        else:
            total_transactions = df.groupby('node_id').size()

        plt.figure(figsize=(10, 6))
        if not total_transactions.empty:
            ax = total_transactions.plot(kind='bar')
            for i, (node_id, v) in enumerate(total_transactions.items()):
                ax.text(i, v, str(node_id), ha='center', va='bottom', fontsize=10, color='blue')

        else:
            plt.bar([], [])

        plt.xlabel('Node ID')
        plt.ylabel('Total Transactions')
        plt.title('Total Transactions Per Node')
        plt.grid(True)

        plt.xticks(rotation=45)

        buf = BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        plt.close()
        return buf

    async def plot_fraud_transactions(self) -> BytesIO:
        transactions = await self.node_fraud_transaction_service.get_all()
        if not transactions:
            fields = [field.alias for field in dto.NodeFraudTransaction.model_fields.values()]
            df = pd.DataFrame(columns=fields)
        else:
            df = pd.DataFrame([vars(ft) for ft in transactions])

        if df.empty or df['node_id'].isnull().all():
            fraud_transactions = pd.Series(dtype='int')
        else:
            fraud_transactions = df.groupby('node_id').size()

        plt.figure(figsize=(10, 6))
        if not fraud_transactions.empty:
            fraud_transactions.plot(kind='bar', color='red')
        else:
            plt.bar([], [], color='red')
        plt.xlabel('Node ID')
        plt.ylabel('Fraud Transactions')
        plt.title('Fraud Transactions Per Node')
        plt.grid(True)

        buf = BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        plt.close()
        return buf

    async def combine_images(self) -> BytesIO:
        images = [
            Image.open(await self.plot_total_transactions()),
            Image.open(await self.plot_fraud_transactions()),
            Image.open(await self.plot_node_ratings()),
        ]
        widths, heights = zip(*(img.size for img in images))

        total_width = max(widths)
        total_height = sum(heights)

        combined_img = Image.new('RGB', (total_width, total_height))

        y_offset = 0
        for img in images:
            combined_img.paste(img, (0, y_offset))
            y_offset += img.height

        combined_buf = BytesIO()
        combined_img.save(combined_buf, format='png')
        combined_buf.seek(0)
        return combined_buf
