from datetime import datetime as dt

from sqlalchemy import String, DateTime, BIGINT
from sqlalchemy.orm import mapped_column, Mapped

from data.models.base_without_id import BaseWithoutId


class Block(BaseWithoutId):
    __tablename__ = 'blocks'

    block_number: Mapped[int] = mapped_column(BIGINT, nullable=False, primary_key=True)
    block_hash: Mapped[str] = mapped_column(String[128], nullable=False)
    parent_hash: Mapped[str] = mapped_column(String[128], nullable=False)
    timestamp: Mapped[dt] = mapped_column(DateTime, nullable=False)
    miner: Mapped[str] = mapped_column(String[48], nullable=False)
    transactions_count: Mapped[int] = mapped_column(BIGINT, nullable=False)

