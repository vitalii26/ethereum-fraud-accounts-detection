import time
from itertools import groupby
from typing import List

from web3 import Web3
import requests
import pandas as pd
from datetime import datetime as dt, timedelta

import dto
from services.base import BaseService
from services.common.account_activity_service import AccountActivityService
from services.common.account_service import AccountService
from services.common.model_handler import ModelHandler
from services.common.node_fraud_transaction_service import NodeFraudTransactionService
from services.common.node_rating_service import NodeRatingService
from services.common.transaction_service import TransactionService


class AnalystService(BaseService):

    def __init__(
            self,
            transaction_service: TransactionService,
            account_service: AccountService,
            node_fraud_transaction_service: NodeFraudTransactionService,
            model_handler: ModelHandler,
            node_rating_service: NodeRatingService,
            account_activity_service: AccountActivityService,
            etherscan_api_key: str,
            infura_url: str,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.web3 = Web3(Web3.HTTPProvider(infura_url))

        self.etherscan_api_key = etherscan_api_key
        self.transaction_service = transaction_service
        self.account_service = account_service
        self.node_fraud_transaction_service = node_fraud_transaction_service
        self.model_handler = model_handler
        self.node_rating_service = node_rating_service
        self.account_activity_service = account_activity_service

    async def collect_account_activities(self):
        accounts = await self.account_service.get_by_updated_at(dt.now() - timedelta(days=1))
        account_activities = []
        for account in accounts:
            try:
                account_activity = self.get_address_stats_normal_tnx(account)
                if account_activity:
                    account_activity = await self.account_activity_service.create_or_update(account_activity)
                    account_activities.append(account_activity)
            except BaseException as e:
                print(e)
                pass

    async def fraud_detect(self):
        account_activities = await self.account_activity_service.get_all()
        prediction_results = self.model_handler.predict(account_activities)

        for prediction_result in prediction_results:
            await self.account_service.update_fraud_probability(prediction_result.account_id,
                                                                prediction_result.probability)

        fraud_accounts = await self.account_service.get_by_high_fraud_probability()
        fraud_account_ids = [account.id for account in fraud_accounts]

        all_old_fraud_transactions = await self.node_fraud_transaction_service.get_all()

        transactions_to_delete = [transaction for transaction in all_old_fraud_transactions if
                                  transaction.account_id not in fraud_account_ids]

        transaction_ids_to_delete = [transaction.id for transaction in transactions_to_delete]
        await self.node_fraud_transaction_service.delete_by_ids(transaction_ids_to_delete)

        if len(fraud_accounts) != 0:
            fraud_transactions = []
            for fraud_account in fraud_accounts:
                transactions = await self.transaction_service.get_by_from_account_address(fraud_account.address)
                fraud_transactions.extend([dto.NodeFraudTransactionCreating(
                    node_id=tr.node_id,
                    transaction_id=tr.id,
                    account_id=next(
                        (account.id for account in fraud_accounts if account.from_address == fraud_account.address),
                        None)
                ) for tr in transactions])

            all_fraud_transactions = await self.node_fraud_transaction_service.create_or_find_many(fraud_transactions)
        else:
            all_fraud_transactions = []

        all_transactions = await self.transaction_service.get_all()

        if not all_transactions:
            grouped_transactions = {}
        else:
            grouped_transactions = {node_id: len(list(group)) for node_id, group in
                                    groupby(all_transactions, key=lambda x: x.node_id)}

        if not all_fraud_transactions:
            grouped_fraud_transactions = {}
        else:
            grouped_fraud_transactions = {node_id: len(list(group)) for node_id, group in
                                          groupby(all_fraud_transactions, key=lambda x: x.node_id)}

        for node_id in grouped_transactions.keys():
            count_all_tx = grouped_transactions.get(node_id, 0)
            count_all_fraud_tx = grouped_fraud_transactions.get(node_id, 0)
            await self.node_rating_service.create(dto.NodeRatingCreating(
                node_id=node_id,
                rating=(count_all_fraud_tx / count_all_tx) * 100 if count_all_tx != 0 else 0,
                timestamp=dt.now()
            ))

    def form_query_string(self, address: str, page: int, offset: int = 10000):
        return "https://api.etherscan.io/api?module=account&action=txlist&address=" + address + "&startblock=0&endblock=99999999&page=" + str(
            page) + "&offset=" + str(offset) + "&sort=asc&apikey=" + self.etherscan_api_key

    def get_address_stats_normal_tnx(self, account: dto.Account) -> dto.AccountActivityCreating | None:
        start_time = time.time()
        result = []
        page = 0
        offset = 10000
        max_retries = 5
        retry_delay = 1

        while True:
            success = False
            res = []
            for attempt in range(max_retries):
                try:
                    response = requests.get(self.form_query_string(account.address, page))
                    if response is not None and response.status_code == 200:
                        res = response.json().get('result', [])
                        success = True
                        break
                    else:
                        print(
                            f"Attempt {attempt + 1} failed: Status code {response.status_code if response else 'None'}")
                except requests.RequestException as e:
                    print(f"Attempt {attempt + 1} failed: {e}")
                    time.sleep(retry_delay)

            if not success:
                print("Max retries exceeded")
                break

            if res:
                result.extend(res)
                page += 1
            else:
                break

            if len(res) != offset:
                break

        if len(result) == 0:
            return None
        print("API request time:", time.time() - start_time)
        sample_df = pd.DataFrame(result)
        # Column creation of ETH from Wei
        sample_df['eth value'] = sample_df['value'].apply(lambda x: Web3.from_wei(int(x), 'ether'))

        sample_df['txn type'] = sample_df['from'].apply(
            lambda x: 'sent' if x.lower() == account.address.lower() else 'received')

        # Handling of Sent transactions stats
        sample_df_sent = sample_df[sample_df['txn type'] == 'sent']
        sample_df_sent = sample_df_sent.sort_values(by=['timeStamp'])
        sample_df_sent['timeStamp'] = sample_df_sent['timeStamp'].astype('int')

        # Compilation of normal sent transaction statistics
        core_stats_Sent_tnx = len(sample_df_sent)
        core_stats_AvgValSent = sample_df_sent['eth value'].dropna().mean() if not sample_df_sent[
            'eth value'].dropna().empty else 0
        core_stats_TotalEtherSent = sample_df_sent['eth value'].dropna().sum() if not sample_df_sent[
            'eth value'].dropna().empty else 0

        # Handling of received transactions stats
        sample_df_received = sample_df[sample_df['txn type'] == 'received']
        sample_df_received = sample_df_received.sort_values(by=['timeStamp'])
        sample_df_received['timeStamp'] = sample_df_received['timeStamp'].astype('int')

        # Compilation of normal received transaction statistics
        core_stats_Received_tnx = len(sample_df_received)
        core_stats_MaxValueReceived = sample_df_received['eth value'].dropna().max() if not sample_df_received[
            'eth value'].dropna().empty else 0
        core_stats_AvgValueReceived = sample_df_received['eth value'].dropna().mean() if not sample_df_received[
            'eth value'].dropna().empty else 0

        # Handling of remaining normal transaction values
        sample_df['timeStamp'] = sample_df['timeStamp'].astype('int')
        sample_df.sort_values(by=['timeStamp'])
        sample_df['unix time difference'] = sample_df['timeStamp'].diff().abs()
        sample_df_time_dim = sample_df.groupby('txn type')['unix time difference'].sum() / 60

        # Compilation of remaining normal transaction statistics
        core_stats_TimeDiffbetweenfirstand_last = ((sample_df['timeStamp'].max()) - (sample_df['timeStamp'].min())) / 60
        core_stats_NumberofCreated_Contracts = len(sample_df[sample_df['contractAddress'] != ''])
        core_stats_Avg_min_between_received_tnx = sample_df_time_dim[
                                                      'received'] / core_stats_Received_tnx if core_stats_Received_tnx != 0 else 0

        core_stats_Avg_min_between_sent_tnx = sample_df_time_dim.get('sent',
                                                                     0) / core_stats_Sent_tnx if core_stats_Sent_tnx != 0 else 0

        checksum_address = Web3.to_checksum_address(account.address)
        balance = self.web3.eth.get_balance(checksum_address)

        return dto.AccountActivityCreating(
            account_id=account.id,
            avg_min_between_sent_tnx=core_stats_Avg_min_between_sent_tnx,
            avg_min_between_received_tnx=core_stats_Avg_min_between_received_tnx,
            time_diff_between_first_and_last=core_stats_TimeDiffbetweenfirstand_last,
            sent_tnx=core_stats_Sent_tnx,
            received_tnx=core_stats_Received_tnx,
            number_of_created_contracts=core_stats_NumberofCreated_Contracts,
            max_val_received=core_stats_MaxValueReceived,
            avg_val_received=core_stats_AvgValueReceived,
            avg_val_sent=core_stats_AvgValSent,
            total_ether_sent=core_stats_TotalEtherSent,
            total_ether_balance=Web3.from_wei(balance, "ether"),
            erc20_total_ether_received=0,
            erc20_total_ether_sent=0,
            erc20_total_ether_sent_contract=0,
            erc20_uniq_sent_addr=0,
            erc20_uniq_rec_token_name=0
        )
