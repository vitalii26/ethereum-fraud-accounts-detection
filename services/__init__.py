'''
    Application services i.e. domain logic classes
'''

# Architectural tips.

# Service layers:
#   1. utils/   - utilitary low-level services. Should not contait any high level business logic
#   2. common/  - general-purpose domain/business logic servies. May contain base classes to higher level services
#   3. deposit/, withdraw/ - high level deposit/withdrawal business logic
#   4. processing_service.py - topmost service, an entry point to main all state machine
#

#                                 (utils)
#                                    |
#                                 (common)
#                                  /    \
#                           (deposit)  (withdraw)
#                                  \    /
#                               (processing)

#  The lower services in this scheme may depends on the higher ones but not the opposite.
