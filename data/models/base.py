'''
    DB base model
'''
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import Mapped


class BaseId(DeclarativeBase):
    '''
        Database table base model
    '''
    id: Mapped[int] = mapped_column(primary_key=True)
