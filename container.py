'''
    Application DI container
'''
from dotenv import load_dotenv
from typing_extensions import Self

from dependency_injector import providers, containers

from background import BlockScanner
from background.analyzer.anylyzer import Analyzer
from background.fraud_detector.fraud_detector import FraudDetector
from data.database import Database
from data.managers.account_activity_manager import AccountActivityManager
from data.managers.account_manager import AccountManager
from data.managers.block_manager import BlockManager
from data.managers.node_fraud_transaction_manager import NodeFraudTransactionManager
from data.managers.node_manager import NodeManager
from data.managers.node_rating_manager import NodeRatingManager
from data.managers.task_semaphore_manager import TaskSemaphoreManager
from data.managers.transaction_manager import TransactionManager
from services.common.account_activity_service import AccountActivityService
from services.common.account_service import AccountService
from services.common.analyst_service import AnalystService
from services.common.block_service import BlockService
from services.common.ethereum_block_scanner import EthereumBlockScanner
from services.common.model_handler import ModelHandler
from services.common.node_fraud_transaction_service import NodeFraudTransactionService
from services.common.node_rating_service import NodeRatingService
from services.common.node_service import NodeService
from services.common.plot_service import PlotService
from services.common.task_semaphore_service import TaskSemaphoreService
from services.common.transaction_service import TransactionService
from services.utils.log_service import LogService

# load env vars from .env file
load_dotenv()


class AppContainer(containers.DeclarativeContainer):
    # Configuration
    config = providers.Configuration(yaml_files=['config.yml'], strict=True)

    # Database connection

    db = providers.Singleton(
        Database,
        url=config.db.url,
        echo=config.db.echo,
        max_overflow=config.db.max_overflow,
        pool_size=config.db.pool_size,
    )

    # Data Managers

    task_semaphore_manager = providers.Factory(
        TaskSemaphoreManager,
        db=db,
    )

    account_activity_manager = providers.Factory(
        AccountActivityManager,
        db=db,
    )

    account_manager = providers.Factory(
        AccountManager,
        db=db,
    )

    block_manager = providers.Factory(
        BlockManager,
        db=db,
    )

    node_fraud_transaction_manager = providers.Factory(
        NodeFraudTransactionManager,
        db=db,
    )

    node_manager = providers.Factory(
        NodeManager,
        db=db,
    )

    node_rating_manager = providers.Factory(
        NodeRatingManager,
        db=db,
    )

    transaction_manager = providers.Factory(
        TransactionManager,
        db=db,
    )

    # Services

    account_activity_service = providers.Factory(
        AccountActivityService,
        account_activity_manager=account_activity_manager,
    )

    account_service = providers.Factory(
        AccountService,
        account_manager=account_manager,
    )

    block_service = providers.Factory(
        BlockService,
        block_manager=block_manager,
    )

    node_fraud_transaction_service = providers.Factory(
        NodeFraudTransactionService,
        node_fraud_transaction_manager=node_fraud_transaction_manager,
    )

    node_service = providers.Factory(
        NodeService,
        node_manager=node_manager,
    )

    node_rating_service = providers.Factory(
        NodeRatingService,
        node_rating_manager=node_rating_manager,
    )

    transaction_service = providers.Factory(
        TransactionService,
        transaction_manager=transaction_manager,
    )

    log_service = providers.Factory(
        LogService,
        filepath=config.services.log_service.filepath,
        max_lines=config.services.log_service.max_lines
    )

    task_semaphore_service = providers.Factory(
        TaskSemaphoreService,
        task_semaphore_manager=task_semaphore_manager
    )

    model_handler = providers.Factory(
        ModelHandler
    )

    plot_service = providers.Factory(
        PlotService,
        node_rating_service=node_rating_service,
        transaction_service=transaction_service,
        node_fraud_transaction_service=node_fraud_transaction_service
    )

    ethereum_block_scanner = providers.Factory(
        EthereumBlockScanner,
        infura_url=config.services.ethereum_block_scanner.infura_url,
        block_service=block_service,
        transaction_service=transaction_service,
        node_service=node_service,
        account_service=account_service
    )

    analyst_service = providers.Factory(
        AnalystService,
        transaction_service=transaction_service,
        account_service=account_service,
        node_fraud_transaction_service=node_fraud_transaction_service,
        model_handler=model_handler,
        node_rating_service=node_rating_service,
        account_activity_service=account_activity_service,
        etherscan_api_key=config.services.analyst_service.etherscan_api_key,
        infura_url=config.services.ethereum_block_scanner.infura_url,
    )

    # Background Tasks

    block_scanner_task = providers.Factory(
        BlockScanner,
        name=config.background.block_scanner.name,
        interval=config.background.block_scanner.interval,
        log_filename=config.background.block_scanner.filename,
        log_level=config.logging.level,
        log_format=config.logging.format,
        log_datefmt=config.logging.datefmt,
        ethereum_block_scanner=ethereum_block_scanner,
        task_semaphore_service=task_semaphore_service,
    )

    analyzer_task = providers.Factory(
        Analyzer,
        name=config.background.analyzer.name,
        interval=config.background.analyzer.interval,
        log_filename=config.background.analyzer.filename,
        log_level=config.logging.level,
        log_format=config.logging.format,
        log_datefmt=config.logging.datefmt,
        analyst_service=analyst_service,
        task_semaphore_service=task_semaphore_service,
    )

    fraud_detector_task = providers.Factory(
        FraudDetector,
        name=config.background.fraud_detector.name,
        interval=config.background.fraud_detector.interval,
        log_filename=config.background.fraud_detector.filename,
        log_level=config.logging.level,
        log_format=config.logging.format,
        log_datefmt=config.logging.datefmt,
        analyst_service=analyst_service,
        task_semaphore_service=task_semaphore_service,
    )

    # instance

    __instance: Self = None

    @classmethod
    def instance(cls):
        if cls.__instance is None:
            cls.__instance = cls()
            cls.__instance.init_resources()
            cls.__instance.wire(
                modules=[__name__],
                packages=['background', 'api']
            )

        return cls.__instance
