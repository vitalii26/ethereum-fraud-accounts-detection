from typing import List

from sqlalchemy import desc

import dto
from data.managers.block_manager import BlockManager
from dto import Block
from services.base import BaseService
from utils.exceptions import BlockIdNotFoundError


class BlockService(BaseService):

    def __init__(
            self,
            block_manager: BlockManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.block_manager = block_manager

    async def get_all(self) -> List[dto.Block]:
        block_models = await self.block_manager.find()
        return [dto.Block.model_validate(u) for u in block_models]

    async def get_by_id(self, block_number: int) -> dto.Block:
        block_model: dto.Block = await self.block_manager.find_one_or_none(block_number=block_number)
        if block_model is None:
            raise BlockIdNotFoundError(block_number)

        return dto.Block.model_validate(block_model)

    async def get_last(self) -> Block | None:
        block_models = await self.block_manager.find(sort_order="desc", sort_field="block_number")
        if block_models:
            return dto.Block.model_validate(block_models[0])
        else:
            return None

    async def create_or_find(self, block_creating: dto.Block) -> dto.Block:
        block_model = await self.block_manager.find_one_or_none(block_number=block_creating.block_number)
        if block_model is not None:
            return dto.Block.model_validate(block_model)

        result = await self.block_manager.add_one(block_creating)

        return dto.Block(
            **block_creating.model_dump(convert_enums=False)
        )


