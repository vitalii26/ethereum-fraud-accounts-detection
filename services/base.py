'''
    Base service class
'''
import logging

from utils.logging import logger


class BaseService:
    '''
        Base service class
    '''
    def __init__(
        self,
    ):
        pass

    @property
    def logging(self) -> logging.Logger:
        return logger.get()
