'''
    High-level application  utility functions
'''
from datetime import datetime as dt, timezone as tz
from enum import Enum


def srv_now(truncate_tz: bool = True):
    ''' Returns current server UTC time. '''
    res = dt.now(tz.utc)
    if truncate_tz:
        res = res.replace(tzinfo=None)

    return res


def enum_dump(data):
    if isinstance(data, dict):
        return {k: enum_dump(v) for k, v in data.items()}
    elif isinstance(data, list):
        return [enum_dump(item) for item in data]
    elif isinstance(data, Enum):
        return data.value
    else:
        return data
