from data import models
from data.managers.base import SQLAlchemyManager


class NodeRatingManager(SQLAlchemyManager):
    model = models.NodeRating

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
