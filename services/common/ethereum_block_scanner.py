from datetime import datetime as dt, timedelta

from web3 import Web3
from web3.types import BlockData

from dto import Block, TransactionCreating
from services.base import BaseService
from services.common.account_service import AccountService
from services.common.block_service import BlockService
from services.common.node_service import NodeService
from services.common.transaction_service import TransactionService


class EthereumBlockScanner(BaseService):

    def __init__(
            self,
            infura_url: str,
            block_service: BlockService,
            node_service: NodeService,
            account_service: AccountService,
            transaction_service: TransactionService,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.web3 = Web3(Web3.HTTPProvider(infura_url))
        self.block_service = block_service
        self.node_service = node_service
        self.account_service = account_service
        self.transaction_service = transaction_service

    async def get_block_data(self, block_number: int):
        block: BlockData = self.web3.eth.get_block(block_number, full_transactions=True)

        block_data = Block(
            block_number=block.number,
            block_hash=block.hash.hex(),
            parent_hash=block.parentHash.hex(),
            timestamp=dt.utcfromtimestamp(block.timestamp),
            miner=block.miner,
            transactions_count=len(block.transactions)
        )
        node = await self.node_service.create_or_find(block.miner)

        transactions = [
            TransactionCreating(
                tx_hash=tx.hash.hex(),
                node_id=node.id,
                block_number=block.number,
                from_address=tx['from'],
                to_address=tx['to'],
                value=tx['value'] | 0,
                gas=tx['gas'],
                gas_price=tx['gasPrice'],
                timestamp=dt.utcfromtimestamp(block.timestamp)
            ) for tx in block.transactions
        ]
        return block_data, transactions

    async def save_new_blocks(self):
        latest_block_number = self.web3.eth.block_number
        last_saved_block = await self.block_service.get_last()
        last_saved_block_number = last_saved_block.block_number if last_saved_block else latest_block_number - 10

        for block_number in range(last_saved_block_number + 1, latest_block_number + 1):
            block_data, transactions = await self.get_block_data(block_number)
            await self.block_service.create_or_find(block_data)
            await self.transaction_service.create_or_find_many(transactions)

            addresses = set()
            for transaction in transactions:
                if transaction.from_address:
                    addresses.add(transaction.from_address)
                if transaction.to_address:
                    addresses.add(transaction.to_address)
            addresses_list = list(addresses)
            accounts = await self.account_service.create_or_find_many(addresses_list, dt.now() - timedelta(days=1))

            print(f"Saved block {block_number} with {len(transactions)} transactions with {len(accounts)} accounts")
