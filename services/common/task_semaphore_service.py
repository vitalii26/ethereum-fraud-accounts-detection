'''
    Background task semaphore utils service
'''
from utils.enums import ExecutableTask
from utils.exceptions import SemaphoreNotFoundError
from data.managers.task_semaphore_manager import TaskSemaphoreManager
from services.base import BaseService
from data import models


class TaskSemaphoreService(BaseService):
    '''
        Background task semaphore utils service
    '''
    def __init__(
            self,
            task_semaphore_manager: TaskSemaphoreManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.task_semaphore_manager = task_semaphore_manager

    async def get_pending(self, name: ExecutableTask) -> bool:
        task_model: models.TaskSemaphore = await self.task_semaphore_manager.find_one_or_none(name=name)
        if task_model is None:
            raise SemaphoreNotFoundError(name)

        return task_model.pending

    async def set_pending(self, name: ExecutableTask, pending: bool):
        task_model: models.TaskSemaphore = await self.task_semaphore_manager.find_one_or_none(name=name)
        if task_model is None:
            raise SemaphoreNotFoundError(name)

        await self.task_semaphore_manager.edit_one(task_model.id, {'pending': pending})

    async def get_enabled(self, name: ExecutableTask) -> bool:
        task_model: models.TaskSemaphore = await self.task_semaphore_manager.find_one_or_none(name=name)
        if task_model is None:
            raise SemaphoreNotFoundError(name)

        return task_model.enabled

    async def set_enabled(self, name: ExecutableTask, enabled: bool):
        task_model: models.TaskSemaphore = await self.task_semaphore_manager.find_one_or_none(name=name)
        if task_model is None:
            raise SemaphoreNotFoundError(name)

        await self.task_semaphore_manager.edit_one(task_model.id, {'enabled': enabled})
