from dto.base import BaseDTO
from datetime import datetime as dt

class AccountCreating(BaseDTO):
    address: str
    fraud_probability: float
    updated_at: dt


class Account(AccountCreating):
    id: int
