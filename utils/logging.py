import sys
import time
import logging
import contextvars
import traceback


logger = contextvars.ContextVar('LOGGER')


def setup_logging(filename: str, level: str, format: str, datefmt: str, logger_name: str = None):
    # set UTC time
    logging.Formatter.converter = time.gmtime

    # stdout handler
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(level)
    stdout_handler.setFormatter(logging.Formatter(format, datefmt=datefmt))

    # file handler
    file_handler = logging.FileHandler(filename)
    file_handler.setLevel(level)
    file_handler.setFormatter(logging.Formatter(format, datefmt=datefmt))

    # Get the root logger
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)
    logger.addHandler(file_handler)
    logger.addHandler(stdout_handler)

    return logger


def trace(err: BaseException):
    return f'\nmessage: {str(err)}\ntrace:\n{traceback.format_exc()}'
