from sqlalchemy import insert

from data import models
from data.managers.base import SQLAlchemyManager


class BlockManager(SQLAlchemyManager):
    model = models.Block

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


    async def add_one(self, data: model) -> dict:
        async with self.session_factory() as session:
            query = insert(self.model).values(**data.model_dump()).returning(self.model.block_number)
            result = await session.execute(query)
            await session.commit()

            return result.mappings().first()
