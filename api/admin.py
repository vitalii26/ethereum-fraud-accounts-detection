'''
    Admin endpoints
'''

from fastapi import APIRouter, Depends
from dependency_injector.wiring import inject, Provide

from services.common.analyst_service import AnalystService
from utils.auth import get_api_key
from utils.enums import LogFile, ExecutableTask
from container import AppContainer
from services.common.task_semaphore_service import TaskSemaphoreService
from services.utils.log_service import LogService
from dto import OK


router = APIRouter(
    prefix='/admin',
    tags=['admin'], )


@router.post('/forceTask', dependencies=[Depends(get_api_key)])
@inject
async def set_task_pending(
        task: ExecutableTask,
        pending: bool,
        task_semaphore_service: TaskSemaphoreService = Depends(Provide[AppContainer.task_semaphore_service])
):
    await task_semaphore_service.set_pending(task, pending)
    return OK


@router.post('/evaluate_nodes')
@inject
async def evaluate_nodes(
        analyst_service: AnalystService = Depends(Provide[AppContainer.analyst_service])
):
    await analyst_service.evaluate_nodes()
    return OK


@router.post('/switchTask', dependencies=[Depends(get_api_key)])
@inject
async def set_task_enabled(
        task: ExecutableTask,
        enabled: bool,
        task_semaphore_service: TaskSemaphoreService = Depends(Provide[AppContainer.task_semaphore_service])
):
    await task_semaphore_service.set_enabled(task, enabled)
    return OK


@router.get('/logs', dependencies=[Depends(get_api_key)])
@inject
async def logs(
    log: LogFile,
    lines: int,
    offset: int = 0,
    log_service: LogService = Depends(Provide[AppContainer.log_service]),
):
    return await log_service.get_server_logs(log, lines, offset)
