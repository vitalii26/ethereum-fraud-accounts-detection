from typing import List

from sqlalchemy import delete

from data import models
from data.managers.base import SQLAlchemyManager


class NodeFraudTransactionManager(SQLAlchemyManager):
    model = models.NodeFraudTransaction

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    async def delete_by_ids(self, transaction_ids: List[int]):
        async with self.session_factory() as session:
            query = delete(self.model).where(self.model.id.in_(transaction_ids))
            await session.execute(query)
            await session.commit()