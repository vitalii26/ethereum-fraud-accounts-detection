'''
    Response DTOs
'''
from dto.base import BaseDTO

from fastapi import status


class Result(BaseDTO):
    ok: bool = True
    code: int = status.HTTP_200_OK
    msg: str | None = None
    value: str | None = None


OK = Result(ok=True)
