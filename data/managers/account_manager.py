from typing import List
from datetime import datetime as dt
from sqlalchemy import select

from data import models
from data.managers.base import SQLAlchemyManager


class AccountManager(SQLAlchemyManager):
    model = models.Account

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    async def get_accounts_by_high_fraud_probability(self) -> List[model]:
        async with self.session_factory() as session:
            query = \
                select(models.Account) \
                    .where(models.Account.fraud_probability >= 0.8)
            result = await session.execute(query)

            return result.scalars().all()

    async def get_by_updated_at(self, updated_at: dt) -> List[model]:
        async with self.session_factory() as session:
            query = \
                select(models.Account) \
                    .where(models.Account.updated_at <= updated_at)
            result = await session.execute(query)

            return result.scalars().all()
