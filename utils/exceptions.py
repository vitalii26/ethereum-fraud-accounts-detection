'''
    App custom exceptions
'''
import logging

from starlette.exceptions import HTTPException
from fastapi import status


class AppException(HTTPException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    detail = 'Critical server error'

    def __init__(self, msg=None):
        super().__init__(status_code=self.status_code, detail=msg or self.detail)

    def __str__(self) -> str:
        return self.__repr__()


class AppWarningException(AppException):
    def __init__(self, msg: str = None, level: int = logging.WARNING):
        super().__init__(msg=msg)
        self.level = level


class AccountExistsError(AppException):
    status_code = status.HTTP_409_CONFLICT

    def __init__(self, address: str):
        super().__init__(msg=f'Account with address "{address}" already exists')


class AccountIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, account_id: int):
        super().__init__(msg=f'Account with id {account_id} not found')


class NodeFraudTransactionIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, node_fraud_transaction_id: int):
        super().__init__(msg=f'NodeFraudTransaction with id {node_fraud_transaction_id} not found')


class NodeRatingIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, node_rating_id: int):
        super().__init__(msg=f'NodeRating with id {node_rating_id} not found')


class NodeIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, node_id: int):
        super().__init__(msg=f'Node with id {node_id} not found')


class NodeNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self):
        super().__init__(msg=f'Node not found')


class TransactionIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, transaction_id: int):
        super().__init__(msg=f'Transaction with id {transaction_id} not found')


class BlockIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, block_id: int):
        super().__init__(msg=f'Block with id {block_id} not found')


class AccountActivityIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, account_activity_id: int):
        super().__init__(msg=f'AccountActivity with id {account_activity_id} not found')


class AccountActivityAccountIdNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, account_activity_account_id: int):
        super().__init__(msg=f'AccountActivity with account_id {account_activity_account_id} not found')


class AccountNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, address: str):
        super().__init__(msg=f'Account with address "{address}" not found')


class SemaphoreNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, name: str):
        super().__init__(msg=f'Semaphore for task "{name}" not found')


class LogFileNotFoundError(AppException):
    status_code = status.HTTP_404_NOT_FOUND

    def __init__(self, filename: int):
        super().__init__(msg=f'Can not find log file "{filename}"')


class Web3Error(AppWarningException):
    def __init__(self, msg: str, level: int):
        super().__init__(msg=f'Web3 lib error: {msg}', level=level)


class ExternalApiError(AppWarningException):
    def __init__(self, msg: str, level: int = logging.WARNING):
        super().__init__(msg=msg, level=level)
