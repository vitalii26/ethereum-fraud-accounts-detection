from datetime import datetime as dt

from sqlalchemy import String, DateTime, BIGINT, Integer, Numeric
from sqlalchemy.orm import Mapped, mapped_column

from data.models.base import BaseId


class Transaction(BaseId):
    __tablename__ = 'transactions'

    tx_hash: Mapped[str] = mapped_column(String[128], nullable=False)
    block_number: Mapped[int] = mapped_column(BIGINT, nullable=False)
    node_id: Mapped[int] = mapped_column(Integer, nullable=False)
    from_address: Mapped[str] = mapped_column(String[48], nullable=True)
    to_address: Mapped[str] = mapped_column(String[48], nullable=True)
    value: Mapped[int] = mapped_column(Numeric, nullable=False)
    gas: Mapped[int] = mapped_column(Numeric, nullable=False)
    gas_price: Mapped[int] = mapped_column(Numeric, nullable=False)
    timestamp: Mapped[dt] = mapped_column(DateTime, nullable=False)
