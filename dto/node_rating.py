from dto.base import BaseDTO
from datetime import datetime as dt


class NodeRatingCreating(BaseDTO):
    node_id: int
    rating: float
    timestamp: dt


class NodeRating(NodeRatingCreating):
    id: int
