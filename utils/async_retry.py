'''
    HTTP calls retry logic
'''
import httpx
import asyncio
from functools import wraps

from starlette.exceptions import HTTPException

from utils.logging import trace, logger


def retry(
    retries: int = None,
    retry_delay: int = None,
    backoff: int = None,
    exceptions=(httpx.RequestError, httpx.HTTPStatusError, HTTPException)
):
    def retry_policy(func):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            _retries = retries if retries is not None else args[0].retries
            _delay = retry_delay if retry_delay is not None else args[0].retry_delay
            _backoff = backoff if backoff is not None else args[0].backoff
            attempt = 0
            while attempt < _retries:
                try:
                    return await func(*args, **kwargs)
                except exceptions as err:
                    attempt += 1
                    if attempt >= _retries:
                        logger.get().error(f'Exceeded {_retries} retries for {func.__name__}: {trace(err)}')
                        raise
                    wait = _delay * (_backoff ** (attempt - 1))
                    logger.get().warning(f'{func.__name__} error: {trace(err)}. Retry in: {wait} seconds...')
                    await asyncio.sleep(wait)
        return wrapper
    return retry_policy
