from typing import List

import dto
from data.managers.transaction_manager import TransactionManager
from services.base import BaseService
from utils.exceptions import TransactionIdNotFoundError


class TransactionService(BaseService):

    def __init__(
            self,
            transaction_manager: TransactionManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.transaction_manager = transaction_manager

    async def get_all(self) -> List[dto.Transaction]:
        transaction_models = await self.transaction_manager.find()
        return [dto.Transaction.model_validate(u) for u in transaction_models]

    async def get_by_id(self, transaction_id: int) -> dto.Transaction:
        transaction_model: dto.Transaction = await self.transaction_manager.find_one_or_none(
            id=transaction_id
        )
        if transaction_model is None:
            raise TransactionIdNotFoundError(transaction_id)

        return dto.Transaction.model_validate(transaction_model)

    async def get_by_node_id(self, node_id: int) -> List[dto.Transaction]:
        transaction_models = await self.transaction_manager.get_transactions_by_node_id(node_id)
        return [dto.Transaction.model_validate(u) for u in transaction_models]

    async def get_by_account_address(self, address: str) -> List[dto.Transaction]:
        transaction_models = await self.transaction_manager.get_transactions_by_address(address)
        return [dto.Transaction.model_validate(u) for u in transaction_models]

    async def get_by_from_account_address(self, address: str) -> List[dto.Transaction]:
        transaction_models = await self.transaction_manager.get_transactions_by_from_address(address)
        return [dto.Transaction.model_validate(u) for u in transaction_models]

    async def create_or_find(self, transaction_creating: dto.TransactionCreating) -> dto.Transaction:
        transaction_model = await self.transaction_manager.find_one_or_none(
            tx_hash=transaction_creating.tx_hash
        )
        if transaction_model is not None:
            return dto.Transaction.model_validate(transaction_model)

        result = await self.transaction_manager.add_one(transaction_creating)
        transaction_id = result['id']

        return dto.Transaction(
            id=transaction_id,
            **transaction_creating.model_dump(convert_enums=False)
        )

    async def create_or_find_many(self, transactions_creating: List[dto.TransactionCreating]) -> List[dto.Transaction]:
        existing_transactions = []
        new_transactions = []

        for transaction_creating in transactions_creating:
            transaction_model = await self.transaction_manager.find_one_or_none(
                tx_hash=transaction_creating.tx_hash
            )
            if transaction_model is not None:
                existing_transactions.append(dto.Transaction.model_validate(transaction_model))
            else:
                new_transactions.append(transaction_creating)

        if new_transactions:
            results = await self.transaction_manager.add_many(new_transactions)
            if len(results) == 1:
                results = [results]

            created_transactions = [
                dto.Transaction(
                    id=result['id'],
                    **transaction_creating.model_dump(convert_enums=False)
                )
                for result, transaction_creating in zip(results, new_transactions)
            ]
            return existing_transactions + created_transactions

        return existing_transactions
