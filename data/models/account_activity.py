from sqlalchemy import Integer, Float
from sqlalchemy.orm import mapped_column, Mapped

from data.models.base import BaseId


class AccountActivity(BaseId):
    __tablename__ = 'account_activities'

    account_id: Mapped[int] = mapped_column(Integer, nullable=False)
    avg_min_between_sent_tnx: Mapped[float] = mapped_column(Float, nullable=True)
    avg_min_between_received_tnx: Mapped[float] = mapped_column(Float, nullable=True)
    time_diff_between_first_and_last: Mapped[float] = mapped_column(Float, nullable=True)
    sent_tnx: Mapped[int] = mapped_column(Integer, nullable=True)
    received_tnx: Mapped[int] = mapped_column(Integer, nullable=True)
    number_of_created_contracts: Mapped[int] = mapped_column(Integer, nullable=True)
    max_val_received: Mapped[float] = mapped_column(Float, nullable=True)
    avg_val_received: Mapped[float] = mapped_column(Float, nullable=True)
    avg_val_sent: Mapped[float] = mapped_column(Float, nullable=True)
    total_ether_sent: Mapped[float] = mapped_column(Float, nullable=True)
    total_ether_balance: Mapped[float] = mapped_column(Float, nullable=True)
    erc20_total_ether_received: Mapped[float] = mapped_column(Float, nullable=True)
    erc20_total_ether_sent: Mapped[float] = mapped_column(Float, nullable=True)
    erc20_total_ether_sent_contract: Mapped[float] = mapped_column(Float, nullable=True)
    erc20_uniq_sent_addr: Mapped[int] = mapped_column(Integer, nullable=True)
    erc20_uniq_rec_token_name: Mapped[int] = mapped_column(Integer, nullable=True)
