'''
    TaskSemaphoreManager table manager
'''
from data.managers.base import SQLAlchemyManager
from data import models


class TaskSemaphoreManager(SQLAlchemyManager):
    '''
        TaskSemaphoreManager table manager
    '''
    model = models.TaskSemaphore

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
