from dto.base import BaseDTO
from datetime import datetime as dt


class Block(BaseDTO):
    block_number: int
    block_hash: str
    parent_hash: str
    miner: str
    timestamp: dt
    transactions_count: int