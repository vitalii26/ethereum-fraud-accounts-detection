'''
    TaskSemaphore DB model
'''
from sqlalchemy import String, Boolean
from sqlalchemy.orm import Mapped, mapped_column

from data.models.base import BaseId


class TaskSemaphore(BaseId):
    __tablename__ = 'task_semaphores'

    name: Mapped[str] = mapped_column(String[64], nullable=False)
    pending: Mapped[bool] = mapped_column(Boolean, nullable=False)
    enabled: Mapped[bool] = mapped_column(Boolean, nullable=False)
