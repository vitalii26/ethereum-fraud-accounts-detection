from background.base import BaseBackgroundTask
from services.common.ethereum_block_scanner import EthereumBlockScanner


class BlockScanner(BaseBackgroundTask):

    def __init__(
            self,
            ethereum_block_scanner: EthereumBlockScanner,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.ethereum_block_scanner = ethereum_block_scanner

    async def run(self, force: bool = False):
        self.logging.info(f'BlockScanner: start iteration (force={force})')

        await self.ethereum_block_scanner.save_new_blocks()

        self.logging.info('BlockScanner: end iteration')
