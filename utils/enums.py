'''
    Appication and DB enums
'''
from typing import Union
from enum import Enum


class SubtaskResult(Enum):
    CONTINUE = 'continue'
    BREAK = 'break'


class ExecutableTask(Enum):
    NOTHING = 'Nothing'


class LogFile(Enum):
    API = 'api'
