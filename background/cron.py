'''
    Cron task scheduler
'''
import asyncio

from background.base import BaseBackgroundTask


class Task:
    '''
        Background task scheduler item
    '''
    def __init__(
            self,
            background_task: BaseBackgroundTask,
            interval: int | None = None,
    ):
        self.background_task = background_task
        self.interval = interval or background_task.interval

    async def start(self):
        await self.background_task.setup()
        while True:
            await asyncio.sleep(self.interval)
            await self.background_task.iter()


class Scheduler:
    '''
        Background task scheduler item
    '''
    def __init__(
            self
    ) -> None:
        self.tasks = []

    def create_task(self, background_task: BaseBackgroundTask) -> None:
        task = Task(background_task)
        self.tasks.append(task)

    async def run_tasks(self):
        tasks = [asyncio.create_task(task.start()) for task in self.tasks]
        await asyncio.gather(*tasks)
