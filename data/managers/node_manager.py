from data import models
from data.managers.base import SQLAlchemyManager


class NodeManager(SQLAlchemyManager):
    model = models.Node

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
