from dto.base import BaseDTO


class NodeCreating(BaseDTO):
    address: str


class Node(NodeCreating):
    id: int
