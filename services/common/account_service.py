from typing import List
from datetime import datetime as dt
from utils.exceptions import AccountNotFoundError, AccountIdNotFoundError
from services.base import BaseService
from data.managers.account_manager import AccountManager

import dto


class AccountService(BaseService):

    def __init__(
            self,
            account_manager: AccountManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.account_manager = account_manager

    async def get_all(self) -> List[dto.Account]:
        account_models = await self.account_manager.find()
        return [dto.Account.model_validate(u) for u in account_models]

    async def get_by_updated_at(self, updated_at: dt) -> List[dto.Account]:
        account_models = await self.account_manager.get_by_updated_at(updated_at)
        if not account_models:
            return []
        return [dto.Account.model_validate(u) for u in account_models]

    async def get_by_id(self, account_id: int) -> dto.Account:
        account_model: dto.Account = await self.account_manager.find_one_or_none(id=account_id)
        if account_model is None:
            raise AccountIdNotFoundError(account_id)

        return dto.Account.model_validate(account_model)

    async def get_by_address(self, address: str) -> dto.Account:
        account_model: dto.Account = await self.account_manager.find_one_or_none(address=address)
        if account_model is None:
            raise AccountNotFoundError(address)

        return dto.Account.model_validate(account_model)

    async def get_by_high_fraud_probability(self) -> List[dto.Account]:
        account_models = await self.account_manager.get_accounts_by_high_fraud_probability()
        if not account_models:
            return []
        return [dto.Account.model_validate(u) for u in account_models]

    async def update_fraud_probability(self, account_id: int, fraud_probability: float):
        account = await self.get_by_id(account_id)
        account.fraud_probability = fraud_probability
        account.updated_at = dt.now()
        await self.account_manager.edit_one(account.id, account.model_dump())

    async def create_or_find(self, address: str) -> dto.Account:
        account_model = await self.account_manager.find_one_or_none(address=address)
        if account_model is not None:
            return dto.Account.model_validate(account_model)

        account_creating = dto.AccountCreating(
            address=address,
            fraud_probability=0.0,
            updated_at=dt.now()
        )

        result = await self.account_manager.add_one(account_creating)
        account_id = result['id']

        return dto.Account(
            id=account_id,
            **account_creating.model_dump(convert_enums=False)
        )

    async def create_or_find_many(self, addresses: List[str], updated_at: dt) -> List[dto.Account]:
        existing_accounts = []
        new_accounts = []

        for address in addresses:
            account_model = await self.account_manager.find_one_or_none(address=address)
            if account_model is not None:
                existing_accounts.append(dto.Account.model_validate(account_model))
            else:
                new_accounts.append(address)

        if new_accounts:
            account_creating_list = [
                dto.AccountCreating(
                    address=address,
                    fraud_probability=0.0,
                    updated_at=updated_at
                )
                for address in new_accounts
            ]
            results = await self.account_manager.add_many(account_creating_list)

            if len(results) == 1:
                results = [results]

            created_accounts = [
                dto.Account(
                    id=result['id'],
                    **account_creating.model_dump(convert_enums=False)
                )
                for result, account_creating in zip(results, account_creating_list)
            ]

            return existing_accounts + created_accounts

        return existing_accounts
