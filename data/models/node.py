from sqlalchemy import String, Float
from sqlalchemy.orm import Mapped, mapped_column

from data.models.base import BaseId


class Node(BaseId):
    __tablename__ = 'nodes'

    address: Mapped[str] = mapped_column(String[48], nullable=False)

