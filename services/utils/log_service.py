'''
    Server logs service
'''
from fastapi import Response

from utils.exceptions import LogFileNotFoundError
from utils.enums import LogFile
from services.base import BaseService


class LogService(BaseService):
    '''
        Server logs service
    '''
    def __init__(
            self,
            filepath: str,
            max_lines: int,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.filepath = filepath
        self.max_lines = max_lines

    async def get_server_logs(self, log: LogFile, lines: int | None, offset: int = 0):
        num_lines = min(lines, self.max_lines)

        try:
            filename = f'{self.filepath}{log.value}.log'
            with open(filename, 'r') as file:
                # move to the end of the file
                file.seek(0, 2)

                # read the file in reverse order
                last_lines = []
                current_line = ''
                skipped_lines = 0
                position = file.tell()

                while (position > 0) and (len(last_lines) < num_lines):
                    position -= 1
                    file.seek(position)
                    char = file.read(1)
                    if char == '\n':
                        if skipped_lines <= offset:
                            skipped_lines += 1
                        else:
                            last_lines.append(current_line[::-1])
                        current_line = ''
                    else:
                        current_line += char

                # add the remaining if any
                if current_line and skipped_lines >= offset:
                    last_lines.append(current_line[::-1])

                last_lines = last_lines[::-1]

                return Response(
                    content='\n'.join(last_lines),
                    media_type='text/plain',
                    headers={
                        'Content-Disposition': f'attachment; filename={log.value}.log'
                    }
                )

        except FileNotFoundError:
            raise LogFileNotFoundError(self.filename)
