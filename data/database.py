'''
    Database connection primitives
'''
from sqlalchemy.ext.asyncio import create_async_engine


class Database:
    '''
        Database connection primitives
    '''

    def __init__(self, url: str, echo: bool = False, max_overflow: int = 10, pool_size: int = 5):
        self.engine = create_async_engine(
            f'postgresql+asyncpg://{url}',
            echo=echo,
            max_overflow=max_overflow,
            pool_size=pool_size)
