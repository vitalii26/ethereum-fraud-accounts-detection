'''
    Application base DTOs
'''
from pydantic import BaseModel
from pydantic import ConfigDict

from utils.utils import enum_dump


class BaseDTO(BaseModel):
    '''
        Application base DTO class
    '''
    model_config = ConfigDict(from_attributes=True)

    def model_dump(self, *args, convert_enums=True, **kwargs):
        data = super().model_dump(*args, **kwargs)
        if convert_enums:
            data = enum_dump(data)

        return data
