from dto.base import BaseDTO


class NodeFraudTransactionCreating(BaseDTO):
    node_id: int
    transaction_id: int
    account_id: int


class NodeFraudTransaction(NodeFraudTransactionCreating):
    id: int
