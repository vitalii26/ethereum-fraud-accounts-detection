from typing import List

import dto
from data.managers.node_rating_manager import NodeRatingManager
from services.base import BaseService
from utils.exceptions import NodeRatingIdNotFoundError


class NodeRatingService(BaseService):

    def __init__(
            self,
            node_rating_manager: NodeRatingManager,
            **kwargs
    ):
        super().__init__(**kwargs)

        self.node_rating_manager = node_rating_manager

    async def get_all(self) -> List[dto.NodeRating]:
        node_rating_models = await self.node_rating_manager.find()
        return [dto.NodeRating.model_validate(u) for u in node_rating_models]

    async def get_by_id(self, node_rating_id: int) -> dto.NodeRating:
        node_rating_model: dto.NodeRating = await self.node_rating_manager.find_one_or_none(
            id=node_rating_id
        )
        if node_rating_model is None:
            raise NodeRatingIdNotFoundError(node_rating_id)

        return dto.NodeRating.model_validate(node_rating_model)

    async def create_or_update(
            self,
            node_rating_creating: dto.NodeRatingCreating
    ) -> dto.NodeRating:

        node_rating_model = await self.node_rating_manager.find_one_or_none(
            node_id=node_rating_creating.node_id
        )
        if node_rating_model is not None:
            await self.node_rating_manager.edit_one(
                id=node_rating_model.id,
                data=node_rating_creating.model_dump()
            )
            return await self.get_by_id(node_rating_model.id)

        result = await self.node_rating_manager.add_one(node_rating_creating)
        node_rating_id = result['id']

        return dto.NodeRating(
            id=node_rating_id,
            **node_rating_creating.model_dump(convert_enums=False)
        )

    async def create(
            self,
            node_rating_creating: dto.NodeRatingCreating
    ) -> dto.NodeRating:
        result = await self.node_rating_manager.add_one(node_rating_creating)
        node_rating_id = result['id']

        return dto.NodeRating(
            id=node_rating_id,
            **node_rating_creating.model_dump(convert_enums=False)
        )
